const assert = require("assert");

function multi(args) {
  return args.reduce((prev, curr) => prev * curr, 1);
}

describe("multi()", function () {
  const multiTests = [
    { args: [1, 1], expected: 1 },
    { args: [1, 0], expected: 0 },
    { args: [0, 1], expected: 0 },
    { args: [2, 2], expected: 4 },
    { args: [-2, 2], expected: -4 },
    { args: [-2, -2], expected: 4 },
    { args: [0, 0], expected: 0 },
  ];

  multiTests.forEach(({ args, expected }) => {
    it(`correctly multiplies ${args.length} args`, function () {
      const res = multi(args);
      assert.strictEqual(res, expected);
    });
  });
});
