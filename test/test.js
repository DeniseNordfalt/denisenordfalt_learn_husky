const assert = require("assert");

//describes an array of numbers
describe("Array", function () {
  beforeEach(function () {
    console.log("THIS IS BEFORE EACH");
  });
  describe("#indexOf()", function () {
    it("should return -1 when the value is not present", function () {
      assert.equal([1, 2, 3].indexOf(4), -1);
    });
  });
});

// describe("add", function () {
//   after(function () {
//     console.log("THIS IS AFTER");
//   });
//   it("should return the sum of two numbers", function () {
//     assert.equal(add(1, 2), 3);
//   });
// });
